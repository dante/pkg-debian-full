#!/usr/bin/make -f

# This script uses debhelper by Joey Hess
#export DH_VERBOSE=1
DEB=$(CURDIR)/debian/tmp
DEV=debian/libsocksd0-dev

# Aim for the top, adapt if anything should break on the buildds.
DEB_BUILD_MAINT_OPTIONS=	hardening=+all
export DEB_BUILD_MAINT_OPTIONS

override_dh_auto_configure:
	dh_auto_configure -- \
	  --with-socks-conf=/etc/dante.conf \
	  --with-sockd-conf=/etc/danted.conf \
	  --with-pidfile=/var/run/danted.pid \
	  --without-gssapi

execute_after_dh_auto_clean:
	for d in sockd dlib; do \
	  cd $$d; \
	  for i in *; do \
	    if [ -L "$$i" ]; then rm $$i; cp -a ../lib/$$i . ; fi \
	  done; \
	  cd ..; \
	done
	rm -rf example/renamed

execute_before_dh_install:
	cp $(DEB)/usr/share/man/man5/socks.conf.5  $(DEB)/usr/share/man/man5/dante.conf.5
	cp $(DEB)/usr/share/man/man5/sockd.conf.5  $(DEB)/usr/share/man/man5/danted.conf.5
	cp $(DEB)/usr/share/man/man8/sockd.8 $(DEB)/usr/share/man/man8/danted.8
	cp $(DEB)/usr/sbin/sockd $(DEB)/usr/sbin/danted
	mkdir -p example/renamed
	for i in example/sockd*.conf; do \
		sed -e 's/sample sockd.conf/sample danted.conf/' < "$$i" > example/renamed/"`basename $$i | sed -e s/sockd/danted/`"; \
	done
	for i in example/socks*.conf; do \
		sed -e 's/sample socks.conf/sample dante.conf/' < "$$i" > example/renamed/"`basename $$i | sed -e s/socks/dante/`"; \
	done

override_dh_installdocs:
	dh_installdocs -A README doc/README.usage doc/README.survey BUGS CREDITS SUPPORT
	dh_installdocs -pdante-client doc/README.socksify
	dh_installdocs -pdante-client -pdante-server
	dh_installdocs -pdante-server doc/SOCKS4.protocol

execute_after_dh_installchangelogs:
	set -e; for p in $$(dh_listpackages); do \
		install -m 644 NEWS "debian/$$p/usr/share/doc/$$p/"; \
	done

override_dh_installinit:
	dh_installinit -pdante-server --name=danted --error-handler=dante_service_error

override_dh_installsystemd:
	dh_installsystemd -pdante-server --name=danted

override_dh_makeshlibs:
	dh_makeshlibs -- -c4

%:
	dh '$@'
